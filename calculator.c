#include <stdio.h>
#include <math.h>
/*
	Simple calculator;
	operations: +, -, *, /, mod(%);
	if you will use %, you can lose the information;
	write statement as: xxxzyyy, 
	xxx - first operand, yyy - second, z - operation;
*/

float CalcPlus(float x1, float x2)
{
	return x1 + x2;
}

float CalcMinus(float x1, float x2)
{
	return x1 - x2;
}

float CalcMultiply(float x1, float x2)
{
	return x1 * x2;
}

float CalcDivide(float x1, float x2)
{
	return x1 / x2;
}
float CalcMod(int x1, int x2) //there can be lost information
{
	return (float)(x1 % x2);
}

/*
Calc function
if operand is unknown - return Infinty as error;
*/
float Calculator(float x1, float x2, char operand) //exeption, divided by zero
{
	switch(operand)
	{
		case '+': 
			return CalcPlus(x1, x2);
		case '-':
			return CalcMinus(x1, x2);
		case '*':
			return CalcMultiply(x1, x2);
		case '/':
			return CalcDivide(x1,x2);
		case '%': 
			return CalcMod((int)x1, (int)x2);
		default:
			printf("Operand is unknown\n");
			return CalcDivide(1,0); //exeption, divided by zero;
	}
}

int main()
{
	float operatorFirst, operatorSecond, answer;
	char operand, flag;

	printf("Please, input equation as: xxxzyyy.\n");
	while (flag != 'q')
	{
		scanf("%5f%1c%5f", &operatorFirst, &operand, &operatorSecond);
		answer = Calculator(operatorFirst, operatorSecond, operand);
		
		if (isinf(answer)) 
			printf("Error\n\n");

		else 
			printf("Your answer is %f.\n\n", answer);


		scanf("%c", &flag);
		printf("If you want to (q)uit, press 'q'\nFor continue - any other key\n");
	} 
	

	return 0;
}
