#include <stdio.h>
#include <limits.h>
#include <float.h>


int main()
{	
	printf("Char: Min = %i, Max = %i\n", CHAR_MIN, CHAR_MAX);
	printf("Int: Min = %i, Max = %i\n", INT_MIN, INT_MAX);
	printf("Long Int: Min = %li, Max = %li\n", LONG_MIN, LONG_MAX);
	printf("Float: Min = %f, Max = %f\n", FLT_MIN, FLT_MAX);
	printf("Double: Min = %f, Max = %f\n", DBL_MIN, DBL_MAX);
	return 0;
}


